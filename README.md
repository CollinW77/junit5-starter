# Intro to this Project

This is a starter for a Java project that uses Gradle as its build tool,
JUnit as its testing framework, and Docker.

## The Problems

What does it mean to build a project?

1. Compiling source code into machine code.
2. Linking machine code into an executable.
3. Compiling the test source code into machine code.
4. Linking test machine code with application machine code.
5. Preparing resources (e.g., images, data files, etc.)
6. Running the tests.
7. Running linters and other automated static analysis tools.
8. Generating documentation (e.g., from javadocs)
9. Bundling the above into a bundle/installer that can be shipped to clients.
10. Testing the package.

It's important not to forget a step, and to perform the above in the right
order every time -- otherwise you'll get false positives (slows development)
or false negatives (ships buggy code).

In a large system, the above could be very slow. Maybe there are optimizations
to speed this up. For example, if you change one line of one file, must the
entire system be recompiled? Must all the tests be re-ran? Must the resources
be reprocessed? Must the entire bundle/installer be repackaged?

Projects typically have 3rd party dependencies, and those dependencies have
dependencies. If another developer tries to build and run the project, they
will need to download and install all of these dependencies, and they need
to get the right versions of the packages (newer versions may not work). It
would be better if dependency management was automated.

Build automation tools are used to help automate much of the above.

## Gradle

Gradle is a popular build automation tool for Java and other languages and
frameworks in the Java family (e.g, Groovy, Kotlin, Android, etc.; other
languages are possible, but Gradle is most used in the Java community).

## Using Gradle through Docker

    docker run -it --rm -v "${PWD}:/w" -w /w gradle:7.0-jdk11 gradle

This command is wrapped in the `gradled` script and the
`gradled.bat` for Windows. So you can run the above command
using

    gradled

Command-line arguments passed to these scripts are passed through to the
above command. So you can use it as you would `gradle` or `gradlew`. So
if you see documentation referring to `gradle` or `gradlew`, replace it
with `gradled`.

    gradled --version

## Using Gradle

For a list of common tasks see https://docs.gradle.org/current/userguide/command_line_interface.html#common_tasks

We'll regularly use the following command

    gradled --continuous check --info

This will compile the application, compile the tests, run the tests,
and display the output. The `--continous` option will cause the command to
not exit, and watch for changes to the application or test code and recompile
and retest as necessary. The `--info` option cause the command to produce
more detailed output, which produces more debug information when a test fails.
Without it, you won't know why an assertion failed, just that it failed.

## File Structure

Gradle assumes the following structure

```
src/main/java - The root of all application source code written in java.
src/test/java - The root of all test source code written in Java.
```

Within each subdirectory, create subdirectories that reflect the packages
that you want to place your classes in. For example, if you want to place
the code in `Stack.java` in the `my.datastrctures` package, create the
following subdirectories.

```
src/main/java/my/datastructures
src/test/java/my/datastructures
```

And then place `Stack.java` in `src/main/java/my/datastructures` and
`StackTest.java` in `src/test/java/my/datastructures`. In each file place
the following package statement as the first statement in the file.

```java
package my.datastructures;
```

## Other Files and Directories

```
.gradle/ - Gradle stores its operational data here. When Gradle is not in use,
           this directory may be deleted. It should not be committed to version
           control.

build/ - All yproducts of building, testing, etc. are placed here. It may safely
         be deleted as it can always be regenerated from source code. It should
         not be committed to version control.

build.gradle - Configuration of Gradle. If you project depends on 3rd party
               libraries, declare them and their versions here. Gradle will
               install them when next ran.
```

## `.gradlew.backup/`

Typically, projects managed by Gradle come with the Gradle Wrapper `gradlew`
(`gradlew.bat` for Windows) and its supporting files in `gradle/`. The wrapper
allows one to run Gradle without having to install it first. Of course, an
appropriate Java JDK must be installed first. That's why we are using Docker
instead.

However, if Docker is giving you trouble and you would rather use `gradlew`,
make sure you have Java JDK 11 installed and configured properly (i.e., a
JAVA_HOME environment variable correctly defined, and the PATH environment
variable properly configured), and then move all the files and directories
in `.gradlew.backup/` into the root of the project.

    mv .gradlew.backup/* .

Then you should be able to run `./gradelw` (or just `gradlew` on Windows).
